package com.example.agr.w2g.Places_frag;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agr.w2g.DB.DBHelper.DataBaseHelper;
import com.example.agr.w2g.DB.Model.Place;
import com.example.agr.w2g.R;
import com.example.agr.w2g.site;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Agr on 05/11/2016
 */

public class Rest_Fragment extends Fragment {
    //declare table, table row and category name vars
    private TextView name;
    private Button placename;
    private TextView desc;
    private TextView ocu;
    private TextView friends;
    private TableLayout tableLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.site_list, container);

        name = (TextView) view.findViewById(R.id.textView9);
        name.setText("Restaurants");
        name.setTextSize(30);

        tableLayout=(TableLayout)view.findViewById(R.id.tableLayout);
        Generate_List();
        return view;
    }

    private void Generate_List(){
        //we declare the dbhelper in order to be able to access to our database
        DataBaseHelper dbhelper = new DataBaseHelper(getActivity());
        //loop
        tableLayout.removeAllViewsInLayout(); //we need to remove the previous views not to overlap em all
        List<Place> placelist= new ArrayList<>();
        placelist=dbhelper.getPlacebyCatCursor("Restaurants");
        for (Place p : placelist){
            //get the views
            final View tableRow = LayoutInflater.from(getActivity()).inflate(R.layout.frag_site_list, null, false);
            placename = (Button) tableRow.findViewById(R.id.button8);
            desc = (TextView) tableRow.findViewById(R.id.textView3);
            ocu = (TextView) tableRow.findViewById(R.id.textView6);
            friends = (TextView) tableRow.findViewById(R.id.textView5);
            //set the parameters
            placename.setText(p.getName());
            desc.setText(p.getDesc());
            ocu.setText("Ocupation: "+p.getOcu());
            friends.setText("Friends: "+p.getFriends());
            //set the buttons function
            final String lat = String.valueOf(p.getLat());
            final String lng = String.valueOf(p.getLng());
            final String rating = String.valueOf(p.getRat());
            placename.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent place_intent= new Intent();
                    view.getId();
                    clicked(((Button)tableRow.findViewById(R.id.button8)).getText().toString());
                    place_intent.setClass(getActivity().getApplication(),site.class);
                    place_intent.putExtra("Name",((TextView)tableRow.findViewById(R.id.button8)).getText().toString());
                    place_intent.putExtra("Desc",((TextView)tableRow.findViewById(R.id.textView3)).getText().toString());
                    place_intent.putExtra("Ocu",((TextView)tableRow.findViewById(R.id.textView6)).getText().toString());
                    place_intent.putExtra("Friends",((TextView)tableRow.findViewById(R.id.textView5)).getText().toString());
                    if (rating.equals("10"))
                        place_intent.putExtra("Rating","NA");
                    else
                        place_intent.putExtra("Rating", rating);
                    place_intent.putExtra("Lat", lat);
                    place_intent.putExtra("Lng", lng);

                    startActivity(place_intent);
                }
            });
            //add rows
            tableLayout.addView(tableRow);
        }
    }
    //we are going to create a toast when moving to the activity, because we can
    private void clicked(String text){
        Toast.makeText(getActivity(),text,Toast.LENGTH_LONG).show();
    }

}
