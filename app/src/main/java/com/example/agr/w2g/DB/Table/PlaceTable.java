package com.example.agr.w2g.DB.Table;

import android.provider.BaseColumns;

/**
 * Created by Agr on 03/11/2016.
 * Contains the structure of the table and the selects and drop operations
 */

public class PlaceTable implements BaseColumns { //basecolumns adds _ID and _count columns

        public static final String NAME = "Name";
        public static final String DESC = "Desc";
        public static final String OCU = "Ocupation";
        public static final String FRIENDS = "Friends";
        public static final String DIRS = "Directions";
        public static final String LAT = "Latitude";
        public static final String LNG = "Longitude";
        public static final String CAT = "Category";
        public static final String RAT = "Rating";

        public static final String TABLE_NAME = "PLACES";

        public static final String CREATE_QUERY = "create table " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY   AUTOINCREMENT, " +
                NAME + " TEXT, " +
                DESC + " TEXT, " +
                OCU + " INTEGER," +
                FRIENDS + " INTEGER," +
                DIRS + " TEXT, " +
                LAT + " REAL, " +
                LNG + " REAL, " +
                CAT + " INTEGER, " +
                RAT + " REAL)";

        public static final String DROP_QUERY = "drop table " + TABLE_NAME;
        public static final String SElECT_ALL_QUERY = "select * from " + TABLE_NAME;
        public static final String SELECT_BAR_QUERY = "select * from " + TABLE_NAME + " where "+CAT+" = 0";
        public static final String SELECT_DISCO_QUERY = "select * from " + TABLE_NAME + " where "+CAT+" = 1";
        public static final String SELECT_PUB_QUERY = "select * from " + TABLE_NAME + " where "+CAT+" = 2";
        public static final String SELECT_REST_QUERY = "select * from " + TABLE_NAME + " where "+CAT+" = 3";
}
