package com.example.agr.w2g.DB.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.agr.w2g.DB.Model.Place;
import com.example.agr.w2g.DB.Model.PlaceComs;
import com.example.agr.w2g.DB.Model.Users;
import com.example.agr.w2g.DB.Table.PlaceComsTable;
import com.example.agr.w2g.DB.Table.PlaceTable;
import com.example.agr.w2g.DB.Table.UsersTable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by Agr on 03/11/2016.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
        public DataBaseHelper(Context context) {
            super(context, "Retail", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(PlaceTable.CREATE_QUERY);
            sqLiteDatabase.execSQL(UsersTable.CREATE_QUERY);
            sqLiteDatabase.execSQL(PlaceComsTable.CREATE_QUERY);
            seedProducts(sqLiteDatabase);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int prevVersion, int newVersion) {
            sqLiteDatabase.execSQL(PlaceTable.DROP_QUERY);
            sqLiteDatabase.execSQL(PlaceTable.CREATE_QUERY);
            sqLiteDatabase.execSQL(PlaceComsTable.DROP_QUERY);
            sqLiteDatabase.execSQL(PlaceComsTable.CREATE_QUERY);
            sqLiteDatabase.execSQL(UsersTable.DROP_QUERY);
            sqLiteDatabase.execSQL(UsersTable.CREATE_QUERY);
        }

        //we are going to create some mocked up stuff in order to test our methods and check that everything is working fine
        private void seedProducts(SQLiteDatabase sqLiteDatabase){
            List<Place> places = asList(
                    new Place("La Traviata", "Ven y pon tu propia música, en Pizarro con terraza", 30, 3, "12312.1231.2312",12.123123,-6.412312,2,4.4),
                    new Place("Las Caballerizas", "¡Buen ambiente y mejor compañía!", 40, 2, "12342.142331.1",12.123123,-6.412312, 2,4.5),
                    new Place("Bar Paco", "Bar de toda la vida, buen ambiente, mejor café", 15, 0, "122.131.312",12.123123,-6.412312, 0,5.0),
                    new Place("Maestro Piero", "Buena comida en una zona tranquila de Cáceres", 20, 2, "312.131.12",12.123123,-6.412312, 3,3.0),
                    new Place("Los Golfines", "Restaurante en la zona antigua de caceres, muy amplio, gran variedad de comida", 35, 0, "32.131.23",12.123123,-6.412312, 3,4.2),
                    new Place("Las Claras", "Gran selección de cervezas, ¡Pregunta por nuestra cerveza artesana!", 30, 3, "12312.1231.2312",12.123123,-6.412312, 2,5.0));

            for (Place place : places) {
                ContentValues values = new ContentValues();
                values.put(PlaceTable.NAME, place.getName());
                values.put(PlaceTable.DESC, place.getDesc());
                values.put(PlaceTable.OCU, place.getOcu());
                values.put(PlaceTable.FRIENDS, place.getFriends());
                values.put(PlaceTable.DIRS, place.getDirs());
                values.put(PlaceTable.LAT, place.getLat());
                values.put(PlaceTable.LNG, place.getLng());
                values.put(PlaceTable.CAT, place.getCat());
                values.put(PlaceTable.RAT, place.getRat());

                sqLiteDatabase.insert(PlaceTable.TABLE_NAME, null, values);
            }
            ContentValues values = new ContentValues();
            values.put(UsersTable.NAME, "HEAVY");
            values.put(UsersTable.PASS,"contra");
            values.put(UsersTable.MAIL,"yop@yop.com");
            sqLiteDatabase.insert(UsersTable.TABLE_NAME,null, values);
        }

    /*PLACE DATABASE HELPER METHODS*/

    //drop a kind of place from the database
        public void drop_place(Integer place){
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(PlaceTable.TABLE_NAME, PlaceTable.CAT + "=" + place, null);
        }

    //get all the places
        public Cursor getPlaceCursor() {
            return this.getReadableDatabase().rawQuery(PlaceTable.SElECT_ALL_QUERY, null);
        }

    //get all the places by category
        public List<Place> getPlacebyCatCursor(String cat) {
            SQLiteDatabase db = this.getReadableDatabase();
            List<Place> placelist=new ArrayList<>();
            Cursor cur;
            switch (cat) {
                case "Bar":
                    cur = db.rawQuery(PlaceTable.SELECT_BAR_QUERY, null);
                    break;
                case "Discos":
                    cur = db.rawQuery(PlaceTable.SELECT_DISCO_QUERY, null);
                    break;
                case "Pubs":
                    cur = db.rawQuery(PlaceTable.SELECT_PUB_QUERY, null);
                    break;
                case "Restaurants":
                    cur = db.rawQuery(PlaceTable.SELECT_REST_QUERY, null);
                    break;
                default:
                    cur = db.rawQuery(PlaceTable.SElECT_ALL_QUERY, null);
            }
            while (cur.moveToNext()) {
                Place place = new Place();
                place.setName(cur.getString(1));
                place.setDesc(cur.getString(2));
                place.setOcu(cur.getInt(3));
                place.setFriends(cur.getInt(4));
                place.setDirs(cur.getString(5));
                place.setLat(cur.getDouble(6));
                place.setLng(cur.getDouble(7));
                place.setCat(cur.getInt(8));
                place.setRat(cur.getDouble(9));
                placelist.add(place);
            }
            cur.close();
            db.close();
            return placelist;
        }

    //add places

        public void addPlace(Place place){

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(PlaceTable.NAME, place.getName());
            values.put(PlaceTable.DESC, place.getDesc());
            values.put(PlaceTable.OCU, place.getOcu());
            values.put(PlaceTable.FRIENDS, place.getFriends());
            values.put(PlaceTable.DIRS, place.getDirs());
            values.put(PlaceTable.LAT, place.getLat());
            values.put(PlaceTable.LNG, place.getLng());
            values.put(PlaceTable.CAT, place.getCat());
            values.put(PlaceTable.RAT, place.getRat());

            db.insert(PlaceTable.TABLE_NAME, null, values);
        }

    /*COMMS DATABASE HELPER METHODS*/
    /*
        Wont be adding the option to update a comment, just because is not within the filosophy of the app
     */

        public long AddComment(String com,String placename){
            SQLiteDatabase db = this.getWritableDatabase();
            long id=0;
            String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
            try ( Cursor test= db.rawQuery("select NAME from PLACES where name like '"+placename+"'",null)) {
                while (test.moveToNext()) {
                    ContentValues values = new ContentValues();
                    values.put(PlaceComsTable.PLACE, placename);
                    values.put(PlaceComsTable.COM, com);
                    values.put(PlaceComsTable.DATE, mydate);
                    id = db.insert(PlaceComsTable.TABLE_NAME, null, values);
                }
                test.close();
                db.close();
            }catch (NullPointerException e) {
                return id;
            }
            return id;
        }

        public PlaceComs getComment(long id){
            SQLiteDatabase db = this.getReadableDatabase();
            PlaceComs com = new PlaceComs();
            Cursor test = db.rawQuery("Select * from PLACES_COMMENTS where _ID ="+id,null);
            if (test.getCount()==0) {
                test.close();
                db.close();
                return null;
            }
            else {
                test.moveToNext();
                com.setId(Integer.valueOf(test.getString(0)));
                com.setComs(test.getString(1));
                com.setPlace(test.getString(2));
                com.setDate(test.getString(3));
                test.close();
                db.close();
                return com;
            }
        }

    public void DelComment(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("PLACES_COMMENTS","_ID="+id,null);
    }

    public List<PlaceComs> getPlaceComentsCursor(String placename){
        SQLiteDatabase db = this.getReadableDatabase();
        List<PlaceComs> comentlis=new ArrayList<>();
        Cursor test =db.rawQuery("select * from PLACES_COMMENTS where PLACE like '"+placename+"'",null);
        if (test.getCount()!=0){
            while (test.moveToNext()) {
                PlaceComs com = new PlaceComs();
                com.setId(Integer.valueOf(test.getString(0)));
                com.setComs(test.getString(1));
                com.setPlace(test.getString(2));
                com.setDate(test.getString(3));
                comentlis.add(com);
            }
            test.close();
            db.close();
        }
        return comentlis;
    }
    /*USER DATABASE HELPER METHODS*/
    //ToDo: modify user
        public Users getUsers(String username, String pass){
            SQLiteDatabase db = this.getReadableDatabase();
            Users user = new Users();
            Cursor test= db.rawQuery("select * from USERS where MAIL like '"+username+"'", null);
            if (test.getCount()==0)
                return null;
            else{
                test.moveToNext();
                user.setName(test.getString(1));
                user.setPass(test.getString(2));
                user.setMail(test.getString(3));
            }
            return user;
        }

        public boolean mail_used(String mail){
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor test = db.rawQuery("select * from USERS where MAIL like '"+mail+"'",null);
            if (test.getCount()==0) return false;
            else return true;
        }
        public boolean name_used(String name){
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor test = db.rawQuery("select * from USERS where NAME like '"+name+"'",null);
            if (test.getCount()==0) return false;
            else return true;
        }

        public Long AddUser (Users user){
            ContentValues values = new ContentValues();
            values.put(UsersTable.NAME, user.getName());
            values.put(UsersTable.PASS, user.getPass());
            values.put(UsersTable.MAIL, user.getMail());
            return this.getWritableDatabase().insert(UsersTable.TABLE_NAME, null, values);
        }

    //ToDo: we need a friends table in order todo the N to N logic of Users
}
