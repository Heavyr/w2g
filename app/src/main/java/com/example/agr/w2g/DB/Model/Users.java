package com.example.agr.w2g.DB.Model;

/**
 * Created by Agr on 03/11/2016.
 */

public class Users {
    private String name;
    private String pass;
    private String mail;
    private String dirs;

    public Users(String name, String pass, String mail, String dirs) {
        this.name = name;
        this.pass = pass;
        this.mail = mail;
        this.dirs = dirs;
    }

    public Users(){}

    public String getName() {
        return name;
    }

    public String getPass() {
        return pass;
    }

    public String getMail () {
        return mail;
    }

    public String getDirs() {
        return dirs;
    }

    public void setName(String name){
        this.name=name;
    }

    public void setPass(String pass){
        this.pass=pass;
    }

    public void setMail(String mail){
        this.mail=mail;
    }

    public void setDirs(String dirs){
            this.dirs = dirs;
    }
}

