package com.example.agr.w2g.DB.Table;

import android.provider.BaseColumns;

/**
 * Created by Agr on 03/11/2016.
 * Contains the structure of the table and the selects and drop operations
 */

public class PlaceComsTable implements BaseColumns { //basecolumns adds _ID and _count columns

        public static final String COM = "Comment";
        public static final String PLACE = "place";
        public static final String DATE= "date";

        public static final String TABLE_NAME = "PLACES_COMMENTS";

        public static final String CREATE_QUERY = "create table " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY   AUTOINCREMENT, " +
                COM + " TEXT, " +
                PLACE + " TEXT," +
                DATE + " TEXT)";

        public static final String DROP_QUERY = "drop table " + TABLE_NAME;

}
