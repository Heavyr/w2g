package com.example.agr.w2g;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import java.util.Calendar;

import static com.example.agr.w2g.R.id.tab;

/**
 * Created by Agr on 30/10/2016.
 */

public class main_page extends AppCompatActivity {
    private float lastX = 0;
    @Override
    protected void onCreate(Bundle savedInsBundle) {
        super.onCreate(savedInsBundle);
        setContentView(R.layout.main_page);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        Init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void Init(){
        final TabHost tbH= (TabHost)findViewById(tab);
        tbH.setup();
        final TabHost.TabSpec tab1 = tbH.newTabSpec("tab1");
        final TabHost.TabSpec tab2 = tbH.newTabSpec("tab2");
        final TabHost.TabSpec tab3 = tbH.newTabSpec("tab3");
        final TabHost.TabSpec tab4 = tbH.newTabSpec("tab4");
        tab1.setIndicator("Bar");
        tab2.setIndicator("Restaurant");
        tab3.setIndicator("Pubs");
        tab4.setIndicator("Disco");
        tab1.setContent(R.id.tab1);
        tab2.setContent(R.id.tab2);
        tab3.setContent(R.id.tab3);
        tab4.setContent(R.id.tab4);

        if (((Calendar.getInstance().get(Calendar.HOUR_OF_DAY)>=7)&&(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<13))||
                ((Calendar.getInstance().get(Calendar.HOUR_OF_DAY)>=15)&&(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<20))){//bar, restaurant, pub, disco
            tbH.addTab(tab1);
            tbH.addTab(tab2);
            tbH.addTab(tab3);
            tbH.addTab(tab4);
        }
        if(((Calendar.getInstance().get(Calendar.HOUR_OF_DAY)>=13)&&(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<15)) ||
                ((Calendar.getInstance().get(Calendar.HOUR_OF_DAY)>=20)&&(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<=22))) {//rest,bar,pub,disco
            tbH.addTab(tab2);
            tbH.addTab(tab1);
            tbH.addTab(tab3);
            tbH.addTab(tab4);
        }
        if((Calendar.getInstance().get(Calendar.HOUR_OF_DAY)>22)|| (Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<3)) {//pub,disco,bar,rest
            tbH.addTab(tab3);
            tbH.addTab(tab4);
            tbH.addTab(tab2);
            tbH.addTab(tab1);

        }
        if((Calendar.getInstance().get(Calendar.HOUR_OF_DAY)>=3)&&(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)<7)){//disco,pub,bar,rest
            tbH.addTab(tab4);
            tbH.addTab(tab3);
            tbH.addTab(tab2);
            tbH.addTab(tab1);
        }
        final TabWidget tw = (TabWidget)tbH.findViewById(android.R.id.tabs);
        for (int i=0; i <tw.getChildCount(); i++){
            final View tabView = tw.getChildAt(i);
            final TextView tv = (TextView)tabView.findViewById(android.R.id.title);
            tv.setTextSize(10);
            tv.setTextColor(Color.parseColor("#FFFFFF"));
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final TabHost mTabHost= (TabHost)findViewById(tab);
        switch (event.getAction()) {
            // when user first touches the screen to swap
            case MotionEvent.ACTION_DOWN: {
                lastX = event.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {
                float currentX = event.getX();
                // if left to right swipe on screen
                if (lastX < currentX - 250) {
                    mTabHost.setCurrentTab(mTabHost.getCurrentTab() - 1);
                }
                // if right to left swipe on screen
                if (lastX > currentX + 250) {
                    mTabHost.setCurrentTab(mTabHost.getCurrentTab() + 1);
                }
                break;
            }
        }
        return false;
    }
}