package com.example.agr.w2g.DB.Table;

import android.provider.BaseColumns;

/**
 * Created by Agr on 09/11/2016.
 */

public class UsersTable implements BaseColumns { //basecolumns adds _ID and _count columns

    public static final String NAME = "Name";
    public static final String PASS = "Pass";
    public static final String MAIL = "Mail";
    public static final String DIRS = "Directions";

    public static final String TABLE_NAME = "USERS";

    public static final String CREATE_QUERY = "create table " + TABLE_NAME + " (" +
            _ID + " INTEGER PRIMARY KEY   AUTOINCREMENT, " +
            NAME + " TEXT, " +
            PASS + " TEXT, " +
            MAIL + " TEXT," +
            DIRS + " TEXT)";

    public static final String DROP_QUERY = "drop table " + TABLE_NAME;
    public static final String SElECT_ALL_QUERY = "select "+NAME+" from " + TABLE_NAME;


}
