package com.example.agr.w2g.DB.Model;

/**
 * Created by Agr on 03/11/2016.
 */

public class PlaceComs {
    private Integer id;
    private String coms;
    private String date;
    private String place;

    public PlaceComs(String coms, String date, String place) {
        this.coms = coms;
        this.date = date;
        this.place = place;
        this.id=0;
    }

    public PlaceComs(){
        this.coms = "";
        this.date = "";
        this.place ="";
        this.id=0;
    }

    public String getComs() {
        return coms;
    }

    public String getDate() {
        return date;
    }

    public String getPlace () {
        return place;
    }

    public Integer getId(){return this.id;}

    public void setId(Integer id){this.id=id;}

    public void setComs(String coms){
        this.coms=coms;
    }

    public void setDate(String date){
        this.date=date;
    }

    public void setPlace(String place){
        this.place=place;
    }


}

