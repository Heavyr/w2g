package com.example.agr.w2g;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.agr.w2g.DB.DBHelper.DataBaseHelper;
import com.example.agr.w2g.DB.Model.Place;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.example.agr.w2g.SettingsActivity.KEY_LATITUDE;
import static com.example.agr.w2g.SettingsActivity.KEY_LONGITUDE;
import static com.example.agr.w2g.SettingsActivity.KEY_TIMESTAMP;

public class MainActivity extends AppCompatActivity {

    private LocationListener locListener = new MyLocationListener();
    private LocationManager locManager;
    private static final int My_app_permission = 1;
    private View load_view;
    private Button quick;
    private Button log;
    private boolean new_pos=false;

    private static final String API_KEY = "AIzaSyDdg6DxiCbxiAtpA9ar0OTdlYVP0GULlUQ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        Init();

        quick.setOnClickListener(new Button.OnClickListener(){
            public void onClick (View V){
                Start();
            }
        });
        log.setOnClickListener(new Button.OnClickListener(){
            public void onClick (View V){
                Login();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void Init(){
        load_view = findViewById(R.id.progressBar);
        log = (Button) findViewById(R.id.button);
        quick = (Button) findViewById(R.id.button2);
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        requestpermision();
    }

    private void Start(){
        Intent intent = new Intent();
        intent.setClass(MainActivity.this,main_page.class);
        startActivity(intent);
    }
    private void Login(){
        Intent intent = new Intent();
        intent.setClass(MainActivity.this,LoginActivity.class);
        startActivity(intent);
    }

//REQUEST GPS PERMISSIONS
    void requestpermision(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION}, My_app_permission);
        }
        else{
            gps_loc();
            check_date();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode){
            case My_app_permission:{
                //if cancelled
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    gps_loc();
                    check_date();
                }
            }
        }
    }

    //GPS LOCATION
    private void gps_loc(){
        try {
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
            Location loc=null;
            if (!new_pos)
                loc=locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (loc!=null) {
                SharedPreferences sp = getSharedPreferences("preferences", MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString(KEY_LATITUDE, String.valueOf(loc.getLatitude()));
                editor.putString(KEY_LONGITUDE, String.valueOf(loc.getLongitude()));
                editor.apply();
                locManager.removeUpdates(locListener);
            }
        }catch (SecurityException e){
        }
    }

    //GPS LISTENER/*
    class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                try {
                    locManager.removeUpdates(locListener);
                }catch (SecurityException e){
                    System.out.println(e);
                }
                SharedPreferences sp = getSharedPreferences("preferences", MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString(KEY_LATITUDE, String.valueOf(location.getLatitude()));
                editor.putString(KEY_LONGITUDE, String.valueOf(location.getLongitude()));
                editor.apply();
                new_pos=true;
            }
        }
        @Override
        public void onProviderDisabled(String provider) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            log.setVisibility(show ? View.GONE : View.VISIBLE);
            log.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    log.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            quick.setVisibility(show ? View.GONE : View.VISIBLE);
            quick.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    quick.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            load_view.setVisibility(show ? View.VISIBLE : View.GONE);
            load_view.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    load_view.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            load_view.setVisibility(show ? View.VISIBLE : View.GONE);
            log.setVisibility(show ? View.GONE : View.VISIBLE);
            quick.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    //IF 30 MINS HAVE PASSED THEN I CHECK AGAING
    private void check_date(){
        SharedPreferences sp = getSharedPreferences("preferences", MODE_PRIVATE);
        Date myDate = new Date(sp.getLong(KEY_TIMESTAMP,0));
        Date current= Calendar.getInstance().getTime();
        if (!sp.getString(KEY_LATITUDE,"").isEmpty())
        if (myDate.getTime() < current.getTime()){//if stored date is lower than current date...
            current.setTime(current.getTime()+((1000*60)*30));
            SharedPreferences.Editor editor = sp.edit();
            editor.putLong(KEY_TIMESTAMP, current.getTime());//stored date is actual date + 30 mins (refresh rate)
            editor.apply();
            showProgress(true);
            new HttpGetRest().execute();
        }
    }

    //-------------------------------------------------------------here we call the asynctask for google places-------------------------------------------------------------
    private class HttpGetRest extends AsyncTask<Void, Void, List<Place>> {
        SharedPreferences sp = getSharedPreferences("preferences", MODE_PRIVATE);
        private final String URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+sp.getString(KEY_LATITUDE, "")+","+sp.getString(KEY_LONGITUDE, "")+"&radius=15000&types=food&name=restaurant&key="
                + API_KEY;

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected List<Place> doInBackground(Void... params) {
            HttpGet request = new HttpGet(URL);
            JSONResponseHandler responseHandler = new JSONResponseHandler();
            try {
                List<Place> list = mClient.execute(request, responseHandler);
                if (null != mClient) {
                    mClient.close();
                    return list;
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != mClient)
                mClient.close();
            return null;
        }

        @Override
        protected void onPostExecute(List<Place> result) {
            if (result != null) {
                DataBaseHelper dbhelper = new DataBaseHelper(getApplicationContext());
                dbhelper.drop_place(3);
                for (int idx = 0; idx < result.size(); idx++) {
                    Place p = result.get(idx);
                    p.setCat(3);
                    p.setFriends(0);
                    p.setOcu(0);
                    dbhelper.addPlace(p);
                }
                dbhelper.close();
                new HttpGetBar().execute();
            }
        }
    }
    private class HttpGetBar extends AsyncTask<Void, Void, List<Place>> {
        SharedPreferences sp = getSharedPreferences("preferences", MODE_PRIVATE);
        private final String URL= "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+sp.getString(KEY_LATITUDE, "")+","+sp.getString(KEY_LONGITUDE,"")+"&radius=15000&types=food&name=bar&key="
                + API_KEY;

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected List<Place> doInBackground(Void... params) {
            HttpGet request = new HttpGet(URL);
            JSONResponseHandler responseHandler = new JSONResponseHandler();
            try {
                List<Place> list = mClient.execute(request, responseHandler);
                if (null != mClient) {
                    mClient.close();
                    return list;
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != mClient)
                mClient.close();
            return null;
        }

        @Override
        protected void onPostExecute(List<Place> result) {
            if (result != null) {
                DataBaseHelper dbhelper = new DataBaseHelper(getApplicationContext());
                dbhelper.drop_place(0);
                for (int idx = 0; idx < result.size(); idx++) {
                    Place p = result.get(idx);
                    p.setCat(0);
                    p.setFriends(0);
                    p.setOcu(0);
                    dbhelper.addPlace(p);
                }
                dbhelper.close();
                new HttpGetPub().execute();
            }
        }
    }

    private class HttpGetPub extends AsyncTask<Void, Void, List<Place>> {
        SharedPreferences sp = getSharedPreferences("preferences", MODE_PRIVATE);
        private final String URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+sp.getString(KEY_LATITUDE, "")+","+sp.getString(KEY_LONGITUDE,"")+"&radius=15000&types=night_club,bar&name=pub&key="
                + API_KEY;

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected List<Place> doInBackground(Void... params) {
            HttpGet request = new HttpGet(URL);
            JSONResponseHandler responseHandler = new JSONResponseHandler();
            try {
                List<Place> list = mClient.execute(request, responseHandler);
                if (null != mClient) {
                    mClient.close();
                    return list;
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != mClient)
                mClient.close();
            return null;
        }

        @Override
        protected void onPostExecute(List<Place> result) {
            if (result != null) {
                DataBaseHelper dbhelper = new DataBaseHelper(getApplicationContext());
                dbhelper.drop_place(2);
                for (int idx = 0; idx < result.size(); idx++) {
                    Place p = result.get(idx);
                    p.setCat(2);
                    p.setFriends(0);
                    p.setOcu(0);
                    dbhelper.addPlace(p);
                }
                dbhelper.close();
                new HttpGetDisco().execute();
            }
        }
    }

    private class HttpGetDisco extends AsyncTask<Void, Void, List<Place>> {
        SharedPreferences sp = getSharedPreferences("preferences", MODE_PRIVATE);
        private final String URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+sp.getString(KEY_LATITUDE, "")+","+sp.getString(KEY_LONGITUDE,"")+"&radius=15000&types=night_club&name=disco&key="
                + API_KEY;

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected List<Place> doInBackground(Void... params) {
            HttpGet request = new HttpGet(URL);
            JSONResponseHandler responseHandler = new JSONResponseHandler();
            try {
                List<Place> list = mClient.execute(request, responseHandler);
                if (null != mClient) {
                    mClient.close();
                    return list;
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null != mClient)
                mClient.close();
            return null;
        }

        @Override
        protected void onPostExecute(List<Place> result) {
            if (result != null) {
                DataBaseHelper dbhelper = new DataBaseHelper(getApplicationContext());
                dbhelper.drop_place(1);
                for (int idx = 0; idx < result.size(); idx++) {
                    Place p = result.get(idx);
                    p.setCat(1);
                    p.setFriends(0);
                    p.setOcu(0);
                    dbhelper.addPlace(p);
                }
                dbhelper.close();
                showProgress(false);
            }
        }
    }


    private class JSONResponseHandler implements ResponseHandler<List<Place>> {
        private static final String RESULTS_TAG = "results";
        private static final String GEOMETRY_TAG = "geometry";
        private static final String LOCATION_TAG = "location";
        private static final String LAT_TAG = "lat";
        private static final String LNG_TAG = "lng";
        private static final String NAME_TAG = "name";
        private static final String RATING_TAG = "rating";
        private static final String DIR_TAG = "vicinity";
        @Override
        public List<Place> handleResponse(HttpResponse response)
                throws ClientProtocolException, IOException {
            List<Place> result = new ArrayList<>();
            String JSONResponse = new BasicResponseHandler()
                    .handleResponse(response);
            try {
                // Get top-level JSON Object - a Map
                JSONObject responseObject = (JSONObject) new JSONTokener(
                        JSONResponse).nextValue();
                //Extract value of "results" key -- a List
                JSONArray results = responseObject.getJSONArray(RESULTS_TAG);
                //iterate results list
                for (int idx = 0; idx < results.length(); idx++) {
                    JSONObject place = (JSONObject) results.get(idx);
                    JSONObject place_geometry = (JSONObject) place.get(GEOMETRY_TAG);
                    JSONObject place_location = (JSONObject) place_geometry.get(LOCATION_TAG);
                    Place p = new Place();
                    p.setName((String) place.get(NAME_TAG));
                    if (place.has(RATING_TAG)) {//not all places has rating, if they dont have rating we show it with 'NA'
                        p.setRat(Double.parseDouble(place.getString(RATING_TAG)));//we treat it like a string instead of an object since it can either be integer or double
                    }else                                                           //and we need to parse it to Double (integer cannot be parsed)
                        p.setRat(10.0);
                    p.setLat(place_location.getDouble(LAT_TAG));
                    p.setLng(place_location.getDouble(LNG_TAG));
                    p.setDirs((String) place.get(DIR_TAG));
                    p.setCat(99);
                    p.setFriends(0);
                    p.setOcu(0);
                    p.setDesc("");
                    result.add(p);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }
    }
}
