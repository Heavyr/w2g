package com.example.agr.w2g;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.agr.w2g.DB.DBHelper.DataBaseHelper;
import com.example.agr.w2g.DB.Model.PlaceComs;

import java.util.List;

import static com.example.agr.w2g.R.id.editText;

/**
 * Created by Agr on 30/10/2016.
 */

public class site extends AppCompatActivity {
    private TextView name;
    private Button addcomment;
    private Button getdirs;
    private ComentsAdapter comentsAdapter;
    private RecyclerView comlist;
    private EditText commentxt;
    private String comstr;
    private TextView rating;
    @Override
    protected  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.site);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        Init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void Init(){
        name = (TextView) findViewById(R.id.textView);
        getdirs = (Button) findViewById(R.id.button7);
        comlist= (RecyclerView)findViewById(R.id.comslist);
        addcomment = (Button) findViewById(R.id.button5);
        rating = (TextView) findViewById(R.id.rating);

        Intent intent = getIntent();

        name.setText(intent.getExtras().getString("Name"));
        name.setTextSize(30);
        rating.setText(intent.getExtras().getString("Rating"));
        comentsAdapter = new ComentsAdapter(this,Generate_Coments());
        comlist.setAdapter(comentsAdapter);
        comlist.setLayoutManager(new LinearLayoutManager(this));

        getdirs.setOnClickListener(new Button.OnClickListener(){
            public void onClick (View V){
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr="+getIntent().getExtras().getString("Lat")+","+getIntent().getExtras().getString("Lng")));
                startActivity(intent);
            }
        });
    }

    private List<PlaceComs> Generate_Coments(){
        final DataBaseHelper dbhelper = new DataBaseHelper(this);
        final List<PlaceComs> comentlist= dbhelper.getPlaceComentsCursor(getIntent().getExtras().getString("Name"));
        addcomment.setOnClickListener(new Button.OnClickListener(){
            public void onClick (View V){
                commentxt = (EditText) findViewById(editText);
                comstr = commentxt.getText().toString();
                long id=0;
                if (!comstr.isEmpty()) {
                    id=dbhelper.AddComment(comstr,getIntent().getExtras().getString("Name"));
                    //el 0 para que se inserten siempre al principio los comentarios
                    comentlist.add(0,dbhelper.getComment(id));
                    commentxt.setText("");
                    //set focus back to recyclerview
                    comlist.requestFocus();
                    //close keyboard
                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    comentsAdapter.notifyDataSetChanged();
                }
            }
        });
        return comentlist;
    }

}

