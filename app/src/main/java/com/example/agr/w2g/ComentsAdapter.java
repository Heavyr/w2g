package com.example.agr.w2g;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.agr.w2g.DB.DBHelper.DataBaseHelper;
import com.example.agr.w2g.DB.Model.PlaceComs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Agr on 10/11/2016.
 */
public class ComentsAdapter extends RecyclerView.Adapter<ComentsAdapter.ViewHolder> {
    private List<PlaceComs> comlist = new ArrayList<>();
    private LayoutInflater inflater;

    private DataBaseHelper dbhelper;
    public ComentsAdapter(Context context, List<PlaceComs> coms) {
        inflater=LayoutInflater.from(context);
        dbhelper= new DataBaseHelper(inflater.getContext().getApplicationContext());
        this.comlist=coms;
    }
    // Create new views (invoked by the layout manager)
    @Override
    public ComentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.adapter_coment_obj, parent,false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.comment.setText(comlist.get(position).getComs());
        holder.date.setText(comlist.get(position).getDate());
        holder.del.setOnClickListener(new Button.OnClickListener(){
            public void onClick (View V){
                    dbhelper.DelComment(comlist.get(position).getId());
                    dbhelper.close();
                    comlist.remove(position);
                    notifyDataSetChanged();
                }
            });
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return comlist.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView comment;
        private TextView date;
        private Button del;

        public ViewHolder(View itemView) {
            super(itemView);
            comment=(TextView) itemView.findViewById(R.id.comms);
            date=(TextView) itemView.findViewById(R.id.date);
            del=(Button) itemView.findViewById(R.id.del);
        }
    }
}
