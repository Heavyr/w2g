package com.example.agr.w2g.DB.Model;

/**
 * Created by Agr on 03/11/2016.
 */

public class Place {
    private String name;
    private String desc;
    private Integer ocu;
    private Integer friends;
    private String dirs;
    private double lat;
    private double lng;
    private Integer cat;
    private Double rat;

    public Place(){}

    public Place(String name, String desc, Integer ocu, Integer friends, String dirs,double lat, double lng, Integer cat,Double rat) {
        this.name = name;
        this.desc = desc;
        this.ocu = ocu;
        this.friends = friends;
        this.dirs = dirs;
        this.lat = lat;
        this.lng = lng;
        this.cat = cat;
        this.rat = rat;
    }


    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getOcu () {
        return ocu;
    }

    public Integer getFriends () {
        return friends;
    }

    public String getDirs() {
        return dirs;
    }

    public double getLat() {return lat;}

    public double getLng() {return lng;}

    public Integer getCat() {
        return cat;
    }

    public Double getRat() {return rat;}

    public void setName(String name){this.name=name;}

    public void setDesc(String desc){this.desc=desc;}

    public void setOcu(Integer ocu){this.ocu=ocu;}

    public void setFriends(Integer friends){this.friends=friends;}

    public void setDirs(String dirs){this.dirs=dirs;}

    public void setLat(double lat){this.lat=lat;}

    public void setLng(double lng){this.lng=lng;}

    public void setCat(Integer cat){this.cat=cat;}

    public void setRat(Double rat) {this.rat= rat;}
}

